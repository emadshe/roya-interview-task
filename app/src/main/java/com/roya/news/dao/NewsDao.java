package com.roya.news.dao;

import com.roya.news.model.News;

import java.util.List;

import androidx.lifecycle.Observer;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

/**
 * NewsDao is a interface that can be have a method connected with room database.
  * @see NewsDao
 */
@Dao
public interface NewsDao {
    @Query("SELECT * FROM News WHERE id BETWEEN :start AND :end")
    List<News> getAll(int start, int end);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<News> news);
}
