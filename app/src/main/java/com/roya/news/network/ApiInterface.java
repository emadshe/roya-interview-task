package com.roya.news.network;

import com.roya.news.model.BaseNews;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * ApiInterface is a interface that we add all calls we need it for news API.
 * @see ApiInterface
 */
public interface ApiInterface {

    @GET("/api/section/get/1/info/{path}")
    Call<BaseNews> getNews(@Path("path") int path);
}
