package com.roya.news.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BaseNews {

    @SerializedName("section_info")
    @Expose
    public List<News> newsList = null;
}
