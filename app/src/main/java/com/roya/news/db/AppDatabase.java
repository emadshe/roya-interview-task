package com.roya.news.db;

import com.roya.news.dao.NewsDao;
import com.roya.news.model.News;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {News.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract NewsDao newsDao();
}
