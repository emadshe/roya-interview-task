package com.roya.news.ui;

import com.roya.news.Utility;
import com.roya.news.model.News;
import com.roya.news.repository.NewsRepository;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class NewsViewModel extends ViewModel {

    private NewsRepository newsRepository;

    NewsViewModel() {
        newsRepository = NewsRepository.getInstance();
    }

    /**
     * Will check if we need the news from API or room database.
     * @param page The page that will need to load the next page.
     */
    public LiveData<List<News>> getNews(int page) {
        if (Utility.isConnected()) {
            return newsRepository.getNewsApi(page);
        } else {
            return newsRepository.getNewsDB(page);
        }
    }
}
