package com.roya.news.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class News {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public Integer id;

    @ColumnInfo(name = "news_id")
    @SerializedName("news_id")
    @Expose
    public Integer newsId;

    @ColumnInfo(name = "news_title")
    @SerializedName("news_title")
    @Expose
    public String newsTitle;

    @ColumnInfo(name = "section_name")
    @SerializedName("section_name")
    @Expose
    public String sectionName;

    @ColumnInfo(name = "image_link")
    @SerializedName("imageLink")
    @Expose
    public String imageLink;

}
