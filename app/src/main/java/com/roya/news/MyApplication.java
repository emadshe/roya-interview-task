package com.roya.news;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.roya.news.db.AppDatabase;
import com.roya.news.network.ApiClient;
import com.roya.news.network.ApiInterface;

import androidx.room.Room;

public class MyApplication extends Application {

    private static MyApplication application;
    private ApiInterface apiInterface;
    private AppDatabase database;

    public static MyApplication getInstance() {
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        database = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "BaseNews").build();
        Stetho.initializeWithDefaults(this);
    }

    public ApiInterface getApi() {
        return apiInterface;
    }

    public AppDatabase getDatabase() {
        return database;
    }
}
